package ru.iteco.taskmanager.repository;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.jetbrains.annotations.NotNull;

import ru.iteco.taskmanager.entity.Task;

public interface TaskRepository {

    @Update("Update app_task set user_id = #{ownerId}, project_id = #{projectId}, name = #{name}, description = #{description}, "
	    + "dateCreated = #{dateCreated}, dateBegin = #{dateBegin}, dateEnd = #{dateEnd}, readinessStatus = #{readinessStatus} "
	    + "where id = #{id}")
    public void merge(@NotNull final Task task);

    @Insert("Insert into app_task (id, user_id, project_id, name, description, dateCreated, dateBegin, dateEnd, readinessStatus)"
	    + " values (#{id}, #{ownerId}, #{projectId}, #{name}, #{description}, #{dateCreated}, #{dateBegin}, #{dateEnd}, #{readinessStatus})")
    public void persist(@NotNull final Task task);

    @Select("Select * from app_task where id = #{id} limit 1")
    public Task findById(@NotNull final String id);

    @Select("Select * from app_task where project_id = #{projectId} limit 1")
    public Task findByProjectId(@NotNull final String projectId);

    @Select("Select * from app_task where name = #{name} limit 1")
    public Task findByName(@NotNull final String name);

    @Select("Select * from app_task")
    public List<Task> findAll();

    @Select("Select * from app_task where user_id = #{ownerId}")
    public List<Task> findAllByOwnerId(@NotNull final String ownerId);

    @Select("Select * from app_task where name LIKE %#{partOfName}%")
    public List<Task> findAllByPartOfName(@NotNull final String partOfName);

    @Select("Select * from app_task where description LIKE %#{partOfDescription}%")
    public List<Task> findAllByPartOfDescription(@NotNull final String partOfDescription);

    @Delete("Delete from app_task where id = #{id}")
    public void remove(@NotNull final String id);

    @Delete("Delete * from app_task")
    public void removeAll();
}
