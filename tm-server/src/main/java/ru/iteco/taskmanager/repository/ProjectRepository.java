package ru.iteco.taskmanager.repository;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.jetbrains.annotations.NotNull;

import ru.iteco.taskmanager.entity.Project;

public interface ProjectRepository {

    @Update("Update app_project set user_id = #{ownerId}, name = #{name}, description = #{description}, "
	    + "dateCreated = #{dateCreated}, dateBegin = #{dateBegin}, dateEnd = #{dateEnd}, readinessStatus = #{readinessStatus} "
	    + "where id = #{id}")
    void merge(@NotNull final Project project);

    @Insert("Insert into app_project (id, user_id, name, description, dateCreated, dateBegin, dateEnd, readinessStatus)"
	    + " values (#{id}, #{ownerId}, #{name}, #{description}, #{dateCreated}, #{dateBegin}, #{dateEnd}, #{readinessStatus})")
    void persist(@NotNull final Project project);

    @Select("Select * from app_project where id = #{id} limit 1")
    Project findById(@NotNull final String id);

    @Select("Select * from app_project where name = #{name} limit 1")
    Project findByName(@NotNull final String name);

    @Select("Select * from app_project")
    List<Project> findAll();

    @Select("Select * from app_project where user_id = #{ownerId}")
    List<Project> findAllByOwnerId(@NotNull final String ownerId);

    @Select("Select * from app_project where name LIKE %#{partOfName}%")
    List<Project> findAllByPartOfName(@NotNull final String partOfName);

    @Select("Select * from app_project where description LIKE %#{partOfDescription}%")
    List<Project> findAllByPartOfDescription(@NotNull final String partOfDescription);

    @Delete("Delete from app_project where id = #{id}")
    void remove(@NotNull final String id);

    @Delete("Delete * from app_project")
    void removeAll();
}
