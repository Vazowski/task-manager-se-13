package ru.iteco.taskmanager.repository;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.jetbrains.annotations.NotNull;

import ru.iteco.taskmanager.entity.User;

public interface UserRepository {

    @Update("Update app_user set email = #{email}, firstName = #{firstName}, lastName = #{lastName}, "
	    + "login = #{login}, middleName = #{middleName}, passwordHash = #{passwordHash}, phone = #{phone}, "
	    + "roleType = #{roleType} where id = #{id}")
    void merge(@NotNull final User user);

    @Insert("Insert into app_user (id, email, firstName, lastName, login, middleName, passwordHash, phone, roleType)"
	    + " values (#{id}, #{email}, #{firstName}, #{lastName}, #{login}, #{middleName}, #{passwordHash}, #{phone}, #{roleType})")
    void persist(@NotNull final User user);

    @Select("Select * from app_user where id = #{id} limit 1")
    User findById(@NotNull final String id);

    @Select("Select * from app_user where login = #{login} limit 1")
    User findByLogin(@NotNull final String login);

    @Select("Select * from app_user")
    List<User> findAll();
}
