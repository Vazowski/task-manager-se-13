package ru.iteco.taskmanager.repository;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.entity.Session;

public interface SessionRepository {

    @Update("Update app_session set timestamp = #{timestamp}, user_id = #{userId}, signature = #{signature} "
	    + "where id = #{id}")
    void merge(@Nullable final Session session);

    @Insert("Insert into app_session (id, signature, timestamp, user_id) values (#{id}, #{signature}, #{timestamp}, #{userId})")
    void persist(@Nullable final Session session);

    @Select("Select * from app_session where id = #{id} limit 1")
    Session findById(@NotNull final String id);

    @Delete("Delete from app_session where id = #{id}")
    void remove(@NotNull final String id);
}
