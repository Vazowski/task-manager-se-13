package ru.iteco.taskmanager.endpoint;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import org.jetbrains.annotations.Nullable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.iteco.taskmanager.api.endpoint.IProjectEndpoint;
import ru.iteco.taskmanager.api.service.locator.IServiceLocator;
import ru.iteco.taskmanager.entity.Project;
import ru.iteco.taskmanager.entity.Session;
import ru.iteco.taskmanager.util.SignatureUtil;

@Getter
@Setter
@NoArgsConstructor
@WebService(endpointInterface = "ru.iteco.taskmanager.api.endpoint.IProjectEndpoint")
public class ProjectEndpoint implements IProjectEndpoint {

    private IServiceLocator serviceLocator;

    @WebMethod
    public void projectMerge(@WebParam(name = "session") @Nullable final Session session,
	    @WebParam(name = "project") @Nullable final Project project) {
	if (SignatureUtil.validate(session) == null)
	    return;
	serviceLocator.getProjectService().merge(project);
    }

    @WebMethod
    public void projectPersist(@WebParam(name = "session") @Nullable final Session session,
	    @WebParam(name = "project") @Nullable final Project project) {
	if (SignatureUtil.validate(session) == null)
	    return;
	serviceLocator.getProjectService().persist(project);
    }

    @WebMethod
    public @Nullable Project findProjectById(@WebParam(name = "session") @Nullable final Session session,
	    @WebParam(name = "id") @Nullable String id) {
	SignatureUtil.validate(session);
	return serviceLocator.getProjectService().findById(id);
    }

    @WebMethod
    public @Nullable Project findProjectByName(@WebParam(name = "session") @Nullable final Session session,
	    @WebParam(name = "projectName") @Nullable String projectName) {
	SignatureUtil.validate(session);
	return serviceLocator.getProjectService().findByName(projectName);
    }

    @WebMethod
    public @Nullable List<Project> findAllProject(@WebParam(name = "session") @Nullable final Session session) {
	SignatureUtil.validate(session);
	return serviceLocator.getProjectService().findAll();
    }

    @WebMethod
    public @Nullable List<Project> findAllProjectByOwnerId(@WebParam(name = "session") @Nullable final Session session,
	    @WebParam(name = "ownerId") @Nullable final String ownerId) {
	SignatureUtil.validate(session);
	return serviceLocator.getProjectService().findAllByOwnerId(ownerId);
    }

    @WebMethod
    public @Nullable List<Project> findAllProjectByPartOfName(
	    @WebParam(name = "session") @Nullable final Session session,
	    @WebParam(name = "partOfName") @Nullable final String partOfName) {
	SignatureUtil.validate(session);
	return serviceLocator.getProjectService().findAllByPartOfName(partOfName);
    }

    @WebMethod
    public @Nullable List<Project> findAllProjectByPartOfDescription(
	    @WebParam(name = "session") @Nullable final Session session,
	    @WebParam(name = "partOfDescription") @Nullable final String partOfDescription) {
	SignatureUtil.validate(session);
	return serviceLocator.getProjectService().findAllByPartOfDescription(partOfDescription);
    }

    @WebMethod
    public void removeProjectById(@WebParam(name = "session") @Nullable final Session session,
	    @WebParam(name = "id") @Nullable String id) {
	if (SignatureUtil.validate(session) == null)
	    return;
	serviceLocator.getProjectService().remove(id);
    }

    @WebMethod
    public void removeAllProject(@WebParam(name = "session") @Nullable final Session session) {
	if (SignatureUtil.validate(session) == null)
	    return;
	serviceLocator.getProjectService().removeAll();
    }
}
