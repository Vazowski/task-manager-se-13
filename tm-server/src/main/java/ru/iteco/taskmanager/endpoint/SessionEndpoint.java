package ru.iteco.taskmanager.endpoint;

import java.util.Date;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.iteco.taskmanager.api.endpoint.ISessionEndpoint;
import ru.iteco.taskmanager.api.service.locator.IServiceLocator;
import ru.iteco.taskmanager.entity.Session;
import ru.iteco.taskmanager.entity.User;
import ru.iteco.taskmanager.util.HashUtil;
import ru.iteco.taskmanager.util.SignatureUtil;

@Getter
@Setter
@NoArgsConstructor
@WebService(endpointInterface = "ru.iteco.taskmanager.api.endpoint.ISessionEndpoint")
public class SessionEndpoint implements ISessionEndpoint {

    @NotNull
    private IServiceLocator serviceLocator;

    @WebMethod
    public void mergeSession(@WebParam(name = "session") @Nullable final Session session) {
	serviceLocator.getSessionService().merge(session);
    }

    @WebMethod
    public void persistSession(@WebParam(name = "session") @Nullable final Session session) {
	serviceLocator.getSessionService().persist(session);
    }

    @WebMethod
    @Nullable
    public Session signSession(@WebParam(name = "login") @Nullable final String login,
	    @WebParam(name = "password") @Nullable final String password) {
	@NotNull
	final Session session = new Session();
	@Nullable
	final User user = serviceLocator.getUserService().findByLogin(login);
	if (!user.getPasswordHash().equals(HashUtil.getHash(password)))
	    return null;

	@NotNull
	final String userId = user.getId();
	session.setUserId(userId);
	@NotNull
	final Long timeStamp = new Date().getTime();
	session.setTimestamp(timeStamp);
	session.setSignature(SignatureUtil.sign(session));
	return session;
    }

    @WebMethod
    @Nullable
    public Session findById(@WebParam(name = "session") @Nullable final String id) {
	return serviceLocator.getSessionService().findById(id);
    }

    @WebMethod
    public void removeSession(@WebParam(name = "session") @Nullable final Session session) {
	serviceLocator.getSessionService().remove(session.getId());
    }
}
