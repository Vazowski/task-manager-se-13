package ru.iteco.taskmanager.service;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.jetbrains.annotations.Nullable;

import lombok.NoArgsConstructor;
import ru.iteco.taskmanager.api.service.ISessionService;
import ru.iteco.taskmanager.entity.Session;
import ru.iteco.taskmanager.repository.SessionRepository;
import ru.iteco.taskmanager.util.DatabaseUtil;

@NoArgsConstructor
public class SessionService extends AbstractService implements ISessionService {

    @Nullable
    private SqlSessionFactory sqlSessionFactory;

    public void merge(@Nullable final Session session) {
	@Nullable
	SqlSession sqlSession = null;
	try {
	    sqlSessionFactory = DatabaseUtil.getSqlSessionFactory();
	    sqlSession = sqlSessionFactory.openSession();
	    SessionRepository sessionRepository = sqlSession.getMapper(SessionRepository.class);
	    sessionRepository.merge(session);
	    sqlSession.commit();
	} catch (Exception e) {
	    sqlSession.rollback();
	    e.printStackTrace();
	} finally {
	    sqlSession.close();
	}
    }

    public void persist(@Nullable final Session session) {
	@Nullable
	SqlSession sqlSession = null;
	try {
	    sqlSessionFactory = DatabaseUtil.getSqlSessionFactory();
	    sqlSession = sqlSessionFactory.openSession();
	    SessionRepository sessionRepository = sqlSession.getMapper(SessionRepository.class);
	    sessionRepository.persist(session);
	    sqlSession.commit();
	} catch (Exception e) {
	    sqlSession.rollback();
	    e.printStackTrace();
	} finally {
	    sqlSession.close();
	}
    }

    @Nullable
    public Session findById(@Nullable final String id) {
	@Nullable
	Session session = new Session();
	@Nullable
	SqlSession sqlSession = null;
	try {
	    sqlSessionFactory = DatabaseUtil.getSqlSessionFactory();
	    sqlSession = sqlSessionFactory.openSession();
	    SessionRepository sessionRepository = sqlSession.getMapper(SessionRepository.class);
	    session = sessionRepository.findById(id);
	    sqlSession.commit();
	} catch (Exception e) {
	    sqlSession.rollback();
	    e.printStackTrace();
	} finally {
	    sqlSession.close();
	}
	return session;
    }

    public void remove(@Nullable final String id) {
	@Nullable
	SqlSession sqlSession = null;
	try {
	    sqlSessionFactory = DatabaseUtil.getSqlSessionFactory();
	    sqlSession = sqlSessionFactory.openSession();
	    SessionRepository sessionRepository = sqlSession.getMapper(SessionRepository.class);
	    sessionRepository.remove(id);
	    sqlSession.commit();
	} catch (Exception e) {
	    sqlSession.rollback();
	    e.printStackTrace();
	} finally {
	    sqlSession.close();
	}
    }
}
