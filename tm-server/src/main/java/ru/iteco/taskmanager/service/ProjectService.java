package ru.iteco.taskmanager.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import lombok.NoArgsConstructor;
import ru.iteco.taskmanager.api.service.IProjectService;
import ru.iteco.taskmanager.entity.Project;
import ru.iteco.taskmanager.repository.ProjectRepository;
import ru.iteco.taskmanager.util.DatabaseUtil;

@NoArgsConstructor
public final class ProjectService extends AbstractService implements IProjectService {

    @Nullable
    private SqlSessionFactory sqlSessionFactory;

    public void merge(@NotNull final Project project) {
	@Nullable
	SqlSession sqlSession = null;
	try {
	    sqlSessionFactory = DatabaseUtil.getSqlSessionFactory();
	    sqlSession = sqlSessionFactory.openSession();
	    ProjectRepository projectRepository = sqlSession.getMapper(ProjectRepository.class);
	    projectRepository.merge(project);
	    sqlSession.commit();
	} catch (Exception e) {
	    sqlSession.rollback();
	    e.printStackTrace();
	} finally {
	    sqlSession.close();
	}
    }

    public void persist(@NotNull final Project project) {
	@Nullable
	SqlSession sqlSession = null;
	try {
	    sqlSessionFactory = DatabaseUtil.getSqlSessionFactory();
	    sqlSession = sqlSessionFactory.openSession();
	    ProjectRepository projectRepository = sqlSession.getMapper(ProjectRepository.class);
	    projectRepository.persist(project);
	    sqlSession.commit();
	} catch (Exception e) {
	    sqlSession.rollback();
	    e.printStackTrace();
	} finally {
	    sqlSession.close();
	}
    }

    @Nullable
    public Project findById(@NotNull final String id) {
	@Nullable
	Project project = new Project();
	@Nullable
	SqlSession sqlSession = null;
	try {
	    sqlSessionFactory = DatabaseUtil.getSqlSessionFactory();
	    sqlSession = sqlSessionFactory.openSession();
	    ProjectRepository projectRepository = sqlSession.getMapper(ProjectRepository.class);
	    project = projectRepository.findById(id);
	    sqlSession.commit();
	} catch (Exception e) {
	    sqlSession.rollback();
	    e.printStackTrace();
	} finally {
	    sqlSession.close();
	}
	return project;
    }

    @Nullable
    public Project findByName(@NotNull final String name) {
	@Nullable
	Project project = new Project();
	@Nullable
	SqlSession sqlSession = null;
	try {
	    sqlSessionFactory = DatabaseUtil.getSqlSessionFactory();
	    sqlSession = sqlSessionFactory.openSession();
	    ProjectRepository projectRepository = sqlSession.getMapper(ProjectRepository.class);
	    project = projectRepository.findByName(name);
	    sqlSession.commit();
	} catch (Exception e) {
	    sqlSession.rollback();
	    e.printStackTrace();
	} finally {
	    sqlSession.close();
	}
	return project;
    }

    @Nullable
    public List<Project> findAll() {
	@Nullable
	List<Project> listProject = new ArrayList<Project>();
	@Nullable
	SqlSession sqlSession = null;
	try {
	    sqlSessionFactory = DatabaseUtil.getSqlSessionFactory();
	    sqlSession = sqlSessionFactory.openSession();
	    ProjectRepository projectRepository = sqlSession.getMapper(ProjectRepository.class);
	    listProject = projectRepository.findAll();
	    sqlSession.commit();
	} catch (Exception e) {
	    sqlSession.rollback();
	    e.printStackTrace();
	} finally {
	    sqlSession.close();
	}
	return listProject;
    }

    @Nullable
    public List<Project> findAllByOwnerId(@NotNull final String ownerId) {
	@Nullable
	List<Project> listProject = new ArrayList<Project>();
	@Nullable
	SqlSession sqlSession = null;
	try {
	    sqlSessionFactory = DatabaseUtil.getSqlSessionFactory();
	    sqlSession = sqlSessionFactory.openSession();
	    ProjectRepository projectRepository = sqlSession.getMapper(ProjectRepository.class);
	    listProject = projectRepository.findAllByOwnerId(ownerId);
	    sqlSession.commit();
	} catch (Exception e) {
	    sqlSession.rollback();
	    e.printStackTrace();
	} finally {
	    sqlSession.close();
	}
	return listProject;
    }

    @Nullable
    public List<Project> findAllByPartOfName(@NotNull final String partOfName) {
	@Nullable
	List<Project> listProject = new ArrayList<Project>();
	@Nullable
	SqlSession sqlSession = null;
	try {
	    sqlSessionFactory = DatabaseUtil.getSqlSessionFactory();
	    sqlSession = sqlSessionFactory.openSession();
	    ProjectRepository projectRepository = sqlSession.getMapper(ProjectRepository.class);
	    listProject = projectRepository.findAllByPartOfName(partOfName);
	    sqlSession.commit();
	} catch (Exception e) {
	    sqlSession.rollback();
	    e.printStackTrace();
	} finally {
	    sqlSession.close();
	}
	return listProject;
    }

    @Nullable
    public List<Project> findAllByPartOfDescription(@NotNull final String partOfDescription) {
	@Nullable
	List<Project> listProject = new ArrayList<Project>();
	@Nullable
	SqlSession sqlSession = null;
	try {
	    sqlSessionFactory = DatabaseUtil.getSqlSessionFactory();
	    sqlSession = sqlSessionFactory.openSession();
	    ProjectRepository projectRepository = sqlSession.getMapper(ProjectRepository.class);
	    listProject = projectRepository.findAllByPartOfDescription(partOfDescription);
	    sqlSession.commit();
	} catch (Exception e) {
	    sqlSession.rollback();
	    e.printStackTrace();
	} finally {
	    sqlSession.close();
	}
	return listProject;
    }

    public void remove(@NotNull final String id) {
	@Nullable
	SqlSession sqlSession = null;
	try {
	    sqlSessionFactory = DatabaseUtil.getSqlSessionFactory();
	    sqlSession = sqlSessionFactory.openSession();
	    ProjectRepository projectRepository = sqlSession.getMapper(ProjectRepository.class);
	    projectRepository.remove(id);
	    sqlSession.commit();
	} catch (Exception e) {
	    sqlSession.rollback();
	    e.printStackTrace();
	} finally {
	    sqlSession.close();
	}
    }

    public void removeAll() {
	@Nullable
	SqlSession sqlSession = null;
	try {
	    sqlSessionFactory = DatabaseUtil.getSqlSessionFactory();
	    sqlSession = sqlSessionFactory.openSession();
	    ProjectRepository projectRepository = sqlSession.getMapper(ProjectRepository.class);
	    projectRepository.removeAll();
	    sqlSession.commit();
	} catch (Exception e) {
	    sqlSession.rollback();
	    e.printStackTrace();
	} finally {
	    sqlSession.close();
	}
    }
}
