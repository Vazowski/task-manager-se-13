package ru.iteco.taskmanager.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import lombok.NoArgsConstructor;
import ru.iteco.taskmanager.api.service.IUserService;
import ru.iteco.taskmanager.entity.User;
import ru.iteco.taskmanager.repository.UserRepository;
import ru.iteco.taskmanager.util.DatabaseUtil;

@NoArgsConstructor
public final class UserService extends AbstractService implements IUserService {

    @NotNull
    private StringBuilder currentUser = new StringBuilder("root");
    @Nullable
    private SqlSessionFactory sqlSessionFactory;

    public void merge(@NotNull final User user) {
	@Nullable
	SqlSession sqlSession = null;
	try {
	    sqlSessionFactory = DatabaseUtil.getSqlSessionFactory();
	    sqlSession = sqlSessionFactory.openSession();
	    UserRepository userRepository = sqlSession.getMapper(UserRepository.class);
	    userRepository.merge(user);
	    sqlSession.commit();
	} catch (Exception e) {
	    sqlSession.rollback();
	    e.printStackTrace();
	} finally {
	    sqlSession.close();
	}
    }

    public void persist(@NotNull final User user) {
	@Nullable
	SqlSession sqlSession = null;
	try {
	    sqlSessionFactory = DatabaseUtil.getSqlSessionFactory();
	    sqlSession = sqlSessionFactory.openSession();
	    UserRepository userRepository = sqlSession.getMapper(UserRepository.class);
	    userRepository.persist(user);
	    sqlSession.commit();
	} catch (Exception e) {
	    sqlSession.rollback();
	    e.printStackTrace();
	} finally {
	    sqlSession.close();
	}
    }

    @Nullable
    public User findById(@NotNull final String id) {
	@Nullable
	User user = new User();
	@Nullable
	SqlSession sqlSession = null;
	try {
	    sqlSessionFactory = DatabaseUtil.getSqlSessionFactory();
	    sqlSession = sqlSessionFactory.openSession();
	    UserRepository userRepository = sqlSession.getMapper(UserRepository.class);
	    user = userRepository.findById(id);
	    sqlSession.commit();
	} catch (Exception e) {
	    sqlSession.rollback();
	    e.printStackTrace();
	} finally {
	    sqlSession.close();
	}
	return user;
    }

    @Nullable
    public User findByLogin(@NotNull final String login) {
	@Nullable
	User user = new User();
	@Nullable
	SqlSession sqlSession = null;
	try {
	    sqlSessionFactory = DatabaseUtil.getSqlSessionFactory();
	    sqlSession = sqlSessionFactory.openSession();
	    UserRepository userRepository = sqlSession.getMapper(UserRepository.class);
	    user = userRepository.findByLogin(login);
	    sqlSession.commit();
	} catch (Exception e) {
	    sqlSession.rollback();
	    e.printStackTrace();
	} finally {
	    sqlSession.close();
	}
	return user;
    }

    @Nullable
    public List<User> findAll() {
	@Nullable
	List<User> userList = new ArrayList<User>();
	@Nullable
	SqlSession sqlSession = null;
	try {
	    sqlSessionFactory = DatabaseUtil.getSqlSessionFactory();
	    sqlSession = sqlSessionFactory.openSession();
	    UserRepository userRepository = sqlSession.getMapper(UserRepository.class);
	    userList = userRepository.findAll();
	    sqlSession.commit();
	} catch (Exception e) {
	    sqlSession.rollback();
	    e.printStackTrace();
	} finally {
	    sqlSession.close();
	}
	return userList;
    }

    @Nullable
    public String getCurrent() {
	return currentUser.toString();
    }

    public void setCurrent(@NotNull final String login) {
	if (!login.equals(null))
	    this.currentUser = new StringBuilder(login);
    }
}
