package ru.iteco.taskmanager.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import lombok.NoArgsConstructor;
import ru.iteco.taskmanager.api.service.ITaskService;
import ru.iteco.taskmanager.entity.Task;
import ru.iteco.taskmanager.repository.TaskRepository;
import ru.iteco.taskmanager.util.DatabaseUtil;

@NoArgsConstructor
public final class TaskService extends AbstractService implements ITaskService {

    @Nullable
    private SqlSessionFactory sqlSessionFactory;

    public void merge(@NotNull final Task task) {
	@Nullable
	SqlSession sqlSession = null;
	try {
	    sqlSessionFactory = DatabaseUtil.getSqlSessionFactory();
	    sqlSession = sqlSessionFactory.openSession();
	    TaskRepository taskRepository = sqlSession.getMapper(TaskRepository.class);
	    taskRepository.merge(task);
	    sqlSession.commit();
	} catch (Exception e) {
	    sqlSession.rollback();
	    e.printStackTrace();
	} finally {
	    sqlSession.close();
	}
    }

    public void persist(@NotNull final Task task) {
	@Nullable
	SqlSession sqlSession = null;
	try {
	    sqlSessionFactory = DatabaseUtil.getSqlSessionFactory();
	    sqlSession = sqlSessionFactory.openSession();
	    TaskRepository taskRepository = sqlSession.getMapper(TaskRepository.class);
	    taskRepository.persist(task);
	    sqlSession.commit();
	} catch (Exception e) {
	    sqlSession.rollback();
	    e.printStackTrace();
	} finally {
	    sqlSession.close();
	}
    }

    @Nullable
    public Task findById(@NotNull final String id) {
	@Nullable
	Task task = new Task();
	@Nullable
	SqlSession sqlSession = null;
	try {
	    sqlSessionFactory = DatabaseUtil.getSqlSessionFactory();
	    sqlSession = sqlSessionFactory.openSession();
	    TaskRepository taskRepository = sqlSession.getMapper(TaskRepository.class);
	    task = taskRepository.findById(id);
	    sqlSession.commit();
	} catch (Exception e) {
	    sqlSession.rollback();
	    e.printStackTrace();
	} finally {
	    sqlSession.close();
	}
	return task;
    }

    @Nullable
    public Task findByProjectId(@NotNull final String projectId) {
	@Nullable
	Task task = new Task();
	@Nullable
	SqlSession sqlSession = null;
	try {
	    sqlSessionFactory = DatabaseUtil.getSqlSessionFactory();
	    sqlSession = sqlSessionFactory.openSession();
	    TaskRepository taskRepository = sqlSession.getMapper(TaskRepository.class);
	    task = taskRepository.findByProjectId(projectId);
	    sqlSession.commit();
	} catch (Exception e) {
	    sqlSession.rollback();
	    e.printStackTrace();
	} finally {
	    sqlSession.close();
	}
	return task;
    }

    @Nullable
    public Task findByName(@NotNull final String name) {
	@Nullable
	Task task = new Task();
	@Nullable
	SqlSession sqlSession = null;
	try {
	    sqlSessionFactory = DatabaseUtil.getSqlSessionFactory();
	    sqlSession = sqlSessionFactory.openSession();
	    TaskRepository taskRepository = sqlSession.getMapper(TaskRepository.class);
	    task = taskRepository.findByName(name);
	    sqlSession.commit();
	} catch (Exception e) {
	    sqlSession.rollback();
	    e.printStackTrace();
	} finally {
	    sqlSession.close();
	}
	return task;
    }

    @Nullable
    public List<Task> findAll() {
	@Nullable
	List<Task> listTask = new ArrayList<Task>();
	@Nullable
	SqlSession sqlSession = null;
	try {
	    sqlSessionFactory = DatabaseUtil.getSqlSessionFactory();
	    sqlSession = sqlSessionFactory.openSession();
	    TaskRepository taskRepository = sqlSession.getMapper(TaskRepository.class);
	    listTask = taskRepository.findAll();
	    sqlSession.commit();
	} catch (Exception e) {
	    sqlSession.rollback();
	    e.printStackTrace();
	} finally {
	    sqlSession.close();
	}
	return listTask;
    }

    @Nullable
    public List<Task> findAllByOwnerId(@NotNull final String ownerId) {
	@Nullable
	List<Task> listTask = new ArrayList<Task>();
	@Nullable
	SqlSession sqlSession = null;
	try {
	    sqlSessionFactory = DatabaseUtil.getSqlSessionFactory();
	    sqlSession = sqlSessionFactory.openSession();
	    TaskRepository taskRepository = sqlSession.getMapper(TaskRepository.class);
	    listTask = taskRepository.findAllByOwnerId(ownerId);
	    sqlSession.commit();
	} catch (Exception e) {
	    sqlSession.rollback();
	    e.printStackTrace();
	} finally {
	    sqlSession.close();
	}
	return listTask;
    }

    @Nullable
    public List<Task> findAllByPartOfName(@NotNull final String partOfName) {
	@Nullable
	List<Task> listTask = new ArrayList<Task>();
	@Nullable
	SqlSession sqlSession = null;
	try {
	    sqlSessionFactory = DatabaseUtil.getSqlSessionFactory();
	    sqlSession = sqlSessionFactory.openSession();
	    TaskRepository taskRepository = sqlSession.getMapper(TaskRepository.class);
	    listTask = taskRepository.findAllByPartOfName(partOfName);
	    sqlSession.commit();
	} catch (Exception e) {
	    sqlSession.rollback();
	    e.printStackTrace();
	} finally {
	    sqlSession.close();
	}
	return listTask;
    }

    @Nullable
    public List<Task> findAllByPartOfDescription(@NotNull final String partOfDescription) {
	@Nullable
	List<Task> listTask = new ArrayList<Task>();
	SqlSession sqlSession = null;
	try {
	    sqlSessionFactory = DatabaseUtil.getSqlSessionFactory();
	    sqlSession = sqlSessionFactory.openSession();
	    TaskRepository taskRepository = sqlSession.getMapper(TaskRepository.class);
	    listTask = taskRepository.findAllByPartOfDescription(partOfDescription);
	    sqlSession.commit();
	} catch (Exception e) {
	    sqlSession.rollback();
	    e.printStackTrace();
	} finally {
	    sqlSession.close();
	}
	return listTask;
    }

    public void remove(@NotNull final String id) {
	SqlSession sqlSession = null;
	try {
	    sqlSessionFactory = DatabaseUtil.getSqlSessionFactory();
	    sqlSession = sqlSessionFactory.openSession();
	    TaskRepository taskRepository = sqlSession.getMapper(TaskRepository.class);
	    taskRepository.remove(id);
	    sqlSession.commit();
	} catch (Exception e) {
	    sqlSession.rollback();
	    e.printStackTrace();
	} finally {
	    sqlSession.close();
	}
    }

    public void removeAll() {
	SqlSession sqlSession = null;
	try {
	    sqlSessionFactory = DatabaseUtil.getSqlSessionFactory();
	    sqlSession = sqlSessionFactory.openSession();
	    TaskRepository taskRepository = sqlSession.getMapper(TaskRepository.class);
	    taskRepository.removeAll();
	    sqlSession.commit();
	} catch (Exception e) {
	    sqlSession.rollback();
	    e.printStackTrace();
	} finally {
	    sqlSession.close();
	}
    }
}
