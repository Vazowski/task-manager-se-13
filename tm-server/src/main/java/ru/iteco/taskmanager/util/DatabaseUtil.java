package ru.iteco.taskmanager.util;

import java.io.InputStream;
import java.util.Properties;

import javax.sql.DataSource;

import org.apache.ibatis.datasource.pooled.PooledDataSource;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.repository.ProjectRepository;
import ru.iteco.taskmanager.repository.SessionRepository;
import ru.iteco.taskmanager.repository.TaskRepository;
import ru.iteco.taskmanager.repository.UserRepository;

public final class DatabaseUtil {

    public static SqlSessionFactory getSqlSessionFactory() throws Exception {

	final InputStream inputStream = DatabaseUtil.class.getClassLoader().getResourceAsStream("db.properties");
	final Properties property = new Properties();
	property.load(inputStream);

	@Nullable
	final String user = property.getProperty("db.login");
	@Nullable
	final String password = property.getProperty("db.password");
	@Nullable
	final String url = property.getProperty("db.host");
	@Nullable
	final String driver = property.getProperty("db.driver");

	final DataSource dataSource = new PooledDataSource(driver, url, user, password);
	final TransactionFactory transactionFactory = new JdbcTransactionFactory();
	final Environment environment = new Environment("development", transactionFactory, dataSource);
	final Configuration configuration = new Configuration(environment);
	configuration.addMapper(UserRepository.class);
	configuration.addMapper(ProjectRepository.class);
	configuration.addMapper(TaskRepository.class);
	configuration.addMapper(SessionRepository.class);

	return new SqlSessionFactoryBuilder().build(configuration);
    }

}
