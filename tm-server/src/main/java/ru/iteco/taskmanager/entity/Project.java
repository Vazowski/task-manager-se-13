package ru.iteco.taskmanager.entity;

import java.io.Serializable;
import java.util.Date;

import org.jetbrains.annotations.NotNull;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.iteco.taskmanager.enumerate.ReadinessStatus;

@Getter
@Setter
@NoArgsConstructor
public final class Project extends AbstractEntity implements Serializable {

    @NotNull
    private String ownerId;
    @NotNull
    private String name;
    @NotNull
    private String description;
    @NotNull
    private String dateCreated;
    @NotNull
    private String dateBegin;
    @NotNull
    private String dateEnd;
    @NotNull
    private String type;
    @NotNull
    private ReadinessStatus readinessStatus;

    public Project(@NotNull final String id, @NotNull final String ownerId, @NotNull final String name,
	    @NotNull final String description, @NotNull final String dateBegin, @NotNull final String dateEnd) {

	this.id = id;
	this.ownerId = ownerId;
	this.name = name;
	this.description = description;
	this.dateCreated = dateFormat.format(new Date());
	this.dateBegin = dateBegin;
	this.dateEnd = dateEnd;
	this.type = "Project";
	this.readinessStatus = ReadinessStatus.PLANNED;
    }
}
