package ru.iteco.taskmanager.command.user;

import java.util.UUID;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.api.endpoint.ISessionEndpoint;
import ru.iteco.taskmanager.api.endpoint.Session;
import ru.iteco.taskmanager.command.AbstractCommand;

public class UserLoginCommand extends AbstractCommand {

    @Override
    public String command() {
	return "login";
    }

    @Override
    public String description() {
	return "  -  login user";
    }

    @Override
    public void execute() throws Exception {
	while (true) {
	    System.out.print("Login: ");
	    @NotNull
	    final String login = scanner.nextLine();
	    System.out.print("Password: ");
	    @NotNull
	    final String password = scanner.nextLine();

	    @NotNull
	    final ISessionEndpoint sessionEndpoint = serviceLocator.getSessionEndpointService()
		    .getSessionEndpointPort();
	    @Nullable
	    Session session = sessionEndpoint.signSession(login, password);
	    if (session == null) {
		System.out.println("Username or password was incorrect. Try again");
		continue;
	    }
	    session.setId(UUID.randomUUID().toString());
	    serviceLocator.getSessionService().setSession(session);
	    sessionEndpoint.persistSession(session);
	    System.out.println("Done");
	    break;
	}
    }
}
