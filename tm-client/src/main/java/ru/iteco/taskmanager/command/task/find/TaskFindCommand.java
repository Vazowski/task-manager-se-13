package ru.iteco.taskmanager.command.task.find;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.api.endpoint.ITaskEndpoint;
import ru.iteco.taskmanager.api.endpoint.IUserEndpoint;
import ru.iteco.taskmanager.api.endpoint.Session;
import ru.iteco.taskmanager.api.endpoint.Task;
import ru.iteco.taskmanager.api.endpoint.User;
import ru.iteco.taskmanager.command.AbstractCommand;

public final class TaskFindCommand extends AbstractCommand {

    @Override
    public String command() {
	return "task-find";
    }

    @Override
    public String description() {
	return "  -  find one task in project";
    }

    @Override
    public void execute() throws Exception {
	@NotNull
	final IUserEndpoint userEndpoint = serviceLocator.getUserEndpointService().getUserEndpointPort();
	@NotNull
	final ITaskEndpoint taskEndpoint = serviceLocator.getTaskEndpointService().getTaskEndpointPort();
	@Nullable
	final Session session = serviceLocator.getSessionService().getSession();
	if (session == null)
	    return;
	@Nullable
	final User user = userEndpoint.findUserById(session, session.getUserId());
	if (user == null)
	    return;

	System.out.print("Name of task: ");
	@NotNull
	final String inputName = scanner.nextLine();
	@Nullable
	final Task tempTask = taskEndpoint.findTaskByName(session, inputName);

	if (tempTask == null)
	    throw new Exception("No task with same name");
	System.out.println("ID: " + tempTask.getId());
	System.out.println("OwnerID: " + tempTask.getOwnerId());
	System.out.println("ProjectID: " + tempTask.getProjectId());
	System.out.println("Name: " + tempTask.getName());
	System.out.println("Description: " + tempTask.getDescription());
	System.out.println("DateCreated: " + tempTask.getDateCreated());
	System.out.println("DateBegin: " + tempTask.getDateBegin());
	System.out.println("DateEnd: " + tempTask.getDateEnd());
	System.out.println("Status: " + tempTask.getReadinessStatus().toString());
    }
}
