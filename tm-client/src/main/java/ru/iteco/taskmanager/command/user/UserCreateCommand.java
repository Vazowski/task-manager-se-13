package ru.iteco.taskmanager.command.user;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.api.endpoint.IUserEndpoint;
import ru.iteco.taskmanager.api.endpoint.RoleType;
import ru.iteco.taskmanager.api.endpoint.Session;
import ru.iteco.taskmanager.api.endpoint.User;
import ru.iteco.taskmanager.command.AbstractCommand;
import ru.iteco.taskmanager.util.HashUtil;

public final class UserCreateCommand extends AbstractCommand {

    @Override
    public String command() {
	return "user-create";
    }

    @Override
    public String description() {
	return "  -  create new user";
    }

    @Override
    public void execute() throws Exception {
	@NotNull
	final IUserEndpoint userEndpoint = serviceLocator.getUserEndpointService().getUserEndpointPort();
	@Nullable
	final Session session = serviceLocator.getSessionService().getSession();
	if (session == null)
	    return;

	System.out.print("Login: ");
	@NotNull
	final String login = scanner.nextLine();
	@Nullable
	final List<User> userList = userEndpoint.findAllUser(session);

	for (User user : userList) {
	    if (user.getLogin().equals(login)) {
		System.out.println("Login exist");
		return;
	    }
	}

	System.out.print("Password: ");
	@NotNull
	final String password = scanner.nextLine();
	System.out.print("Email: ");
	@NotNull
	final String email = scanner.nextLine();
	System.out.print("First name: ");
	@NotNull
	final String firstName = scanner.nextLine();
	System.out.print("Last name: ");
	@NotNull
	final String lastName = scanner.nextLine();
	System.out.print("Middle name: ");
	@NotNull
	final String middleName = scanner.nextLine();
	System.out.print("Phone: ");
	@NotNull
	final String phone = scanner.nextLine();

	@Nullable
	final User user = new User();
	user.setId(UUID.randomUUID().toString());
	user.setEmail(email);
	user.setFisrtName(firstName);
	user.setLastName(lastName);
	user.setLogin(login);
	user.setLogin(middleName);
	user.setLogin(HashUtil.getHash(password));
	user.setLogin(phone);
	user.setRoleType(RoleType.USER);
	userEndpoint.persist(session, user);
	System.out.println("Done");
    }

    @NotNull
    @Override
    public List<String> getRoles() {
	return new ArrayList<String>() {
	    {
		add("Administrator");
	    }
	};
    }
}
